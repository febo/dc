
package dc.ga;

import static dc.ga.DCCurve.Type.Downturn;
import static dc.ga.DCCurve.Type.DownwardOvershoot;
import static dc.ga.DCCurve.Type.Upturn;
import static dc.ga.DCCurve.Type.UpwardOvershoot;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * DC (Directional Change) event detector.
 * 
 * <p><b>Notes:</b></p>
 * <ul>
 * <li>Overshoot events are only detected when a Upturn/Downturn event is detected;</li> 
 * </ul>
 * 
 * @author Fernando Esteban Barril Otero
 */
public class DCCurve
{
    Event[] events;
    
    Event[] output;
    
    /**
     * 0 = downward overshoot
     * 1 = upward overshoot
     */
    double[] meanRatio = new double[2];

    /**
     * 0 = downward overshoot
     * 1 = upward overshoot
     */
    double[] medianRatio = new double[2];

    public void build(Double[] values, double delta)
    {
        ArrayList<Event> events = new ArrayList<Event>();
        Type event = Type.Upturn;

        Event last = new Event(0, 0, Type.Upturn);
        events.add(last);
        
        output = new Event[values.length];

        double pHigh = 0;
        double pLow = 0;

        int[] indexDC = new int[2]; // DC event indexes
        int[] indexOS = new int[2]; // OS event indexes
        int index = 1;

        for (double value : values)
        {
            if (index == 1)
            {
                // it is the first line

                pHigh = value;
                pLow = value;

                Arrays.fill(indexDC, index);
                Arrays.fill(indexOS, index);
            }
            else if (event == Type.Upturn)
            {
                if (value <= (pHigh * (1 - delta)))
                {
                    last.overshoot = detect(UpwardOvershoot, indexDC, indexOS);

                    adjust(last.overshoot == null ? last : last.overshoot,
                        indexDC, indexOS);

                    event = Downturn;
                    pLow = value;

                    indexDC[1] = index;
                    indexOS[0] = index + 1;

                    last = new Event(indexDC[0], indexDC[1], Downturn);
                    events.add(last);
                }
                else if (pHigh < value)
                {
                    pHigh = value;

                    indexDC[0] = index;
                    indexOS[1] = index - 1;
                }
            }
            else
            {
                if (value >= (pLow * (1 + delta)))
                {
                    last.overshoot = detect(DownwardOvershoot, indexDC, indexOS);

                    adjust(last.overshoot == null ? last : last.overshoot,
                        indexDC, indexOS);

                    event = Upturn;
                    pHigh = value;

                    indexDC[1] = index;
                    indexOS[0] = index + 1;

                    last = new Event(indexDC[0], indexDC[1], Upturn);
                    events.add(last);
                }
                else if (pLow > value)
                {
                    pLow = value;

                    indexDC[0] = index;
                    indexOS[1] = index - 1;
                }
            }

            //output[index - 1] = (byte) (last.type == Type.Upturn ? 1 : 0);
            output[index - 1] = last;

            index++;
        }
        
        // fix start index of events
        /*
        ArrayList<Event> reverse = new ArrayList<Event>(events);
        Collections.reverse(reverse);

        Event last = null;

        for (Event e : reverse)
        {
            if (last != null && e.start == last.start)
            {
                last.start = e.end + 1;
            }

            last = e;
        }
        */

        this.events = events.toArray(new Event[events.size()]);
        // downward overshoots
        meanRatio[0] = 0.0;
        medianRatio[0] = 0.0;

        double meanDownturn = 0.0;
        double meanDownwardOvershoot = 0.0;

        int downturn = 0;
        // upward overshoots
        meanRatio[1] = 0.0;
        medianRatio[1] = 0.0;

        double meanUpturn = 0.0;
        double meanUpwardOvershoot = 0.0;

        int upturn = 0;
        
        ArrayList<Double> downwardRatio = new ArrayList<Double>();
        ArrayList<Double> upwardRatio = new ArrayList<Double>();

        for (Event e : this.events)
        {
            // we ignore the first (artificial) event
            if (e.start > 0)
            {
                double ratio = 0.0;
                
                if (e.overshoot != null)
                {
                    ratio = (e.overshoot.length() / (double) e.length());
                }
    
                if (e.type == Type.Upturn)
                {
                    upturn++;
                    meanRatio[1] += ratio;
                    upwardRatio.add(ratio);
                    
                    meanUpturn += e.length();
                    meanUpwardOvershoot += (e.overshoot != null) ? e.overshoot.length() : 0; 
                }
                else if (e.type == Type.Downturn)
                {
                    downturn++;
                    meanRatio[0] += ratio;
                    downwardRatio.add(ratio);
                    
                    meanDownturn += e.length();
                    meanDownwardOvershoot += (e.overshoot != null) ? e.overshoot.length() : 0; 

                }
            }
        }
        
        //meanRatio[0] /= downturn;
        //meanRatio[1] /= upturn;
        
        meanRatio[0] = (meanDownwardOvershoot / downturn) / (meanDownturn / downturn);
        meanRatio[1] = (meanUpwardOvershoot / upturn) / (meanUpturn / upturn);
        
        //Collections.sort(downwardRatio);
        //Collections.sort(upwardRatio);
        
        //medianRatio[0] = downwardRatio.get(downwardRatio.size() / 2);
        //medianRatio[1] = upwardRatio.get(upwardRatio.size() / 2);
    }

    private Event detect(Type type, int[] indexDC, int[] indexOS)
    {
        // overshoot event must have a start index lower that
        // the DC event start index
        if (indexOS[0] < indexOS[1] && indexOS[0] < indexDC[0])
        {
            return new Event(indexOS[0], indexOS[1], type);
        }

        return null;
    }

    private void adjust(Event last, int[] indexDC, int[] indexOS)
    {
        // we might miss the start of an event
        if (indexDC[0] == last.start)
        {
            indexDC[0] = last.end + 1;
        }
        // we might skip the start of an event when there
        // are repeated values or large increases during an
        // upturn overshoot followed by a downturn event and
        // vice-versa (the overshoot will be invalid since
        // the end index will be smaller than the start index)
        else if (indexDC[0] > (last.end + 1))
        {
            indexDC[0] = (last.end + 1);
        }
    }

    public Event findEvent(int index)
    {
        Event last = null;

        for (Event e : events)
        {
            if (index < e.end)
            {
                break;
            }

            last = e;
        }

        return last;
    }

    public enum Type
    {
        Upturn, Downturn, UpwardOvershoot, DownwardOvershoot;
    }

    public static class Event
    {
        public int start = 0;
        public int end = 0;
        public Type type;
        public Event overshoot;

        public Event(int start, int end, Type type)
        {
            this.start = start;
            this.end = end;
            this.type = type;
        }

        @Override
        public String toString()
        {
            return String.format("%4d %4d   %s", start, end, type);
        }

        public final int length()
        {
            return (end - start) + 1;
        }
    }
}