/**
* This file is part of the directional changes DC+GA project.
*
* DC+GA is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DC+GA is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DC+GA.  If not, see <http://www.gnu.org/licenses/>.

*/

package dc.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FReader {

	public FReader(){

	}

	/**
	 * Reads, filters (into a specified interval) and loads the data from the text file
	 * @param filename The name of the file to be read
	 * 
	 * @return days The data in an ArrayList<Double[]> format
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException 
	 */
	public static ArrayList<Double[]> loadData(String filename, int interval)
			throws FileNotFoundException, IOException, ParseException {

		BufferedReader reader = new BufferedReader(new FileReader(filename));

		String line = null;

		ArrayList<Double[]> days = new ArrayList<Double[]>();
		ArrayList<Double> values = new ArrayList<Double>();
		String timestamp = null;
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
		
		String[] token = reader.readLine().split("\\s");
		Date d1 = format.parse(token[0].trim() + " " + token[1].trim());
		Date d2 = null;

		while ((line = reader.readLine()) != null)
		{
			String[] tokens = line.split("\\s");

			if (timestamp == null)
			{
				timestamp = tokens[0].trim();
			}
			else if (!tokens[0].trim().equals(timestamp))
			{
				days.add(values.toArray(new Double[0]));
				values.clear();

				timestamp = tokens[0].trim();
			}
			
			d2 = format.parse(tokens[0].trim() + " " + tokens[1].trim());
			long diff = d2.getTime() - d1.getTime();
			long diffMinutes = diff / (60 * 1000);
			
			if (diffMinutes >= interval){
				values.add(Double.parseDouble(tokens[2].trim()));
				d1 = format.parse(tokens[0].trim() + " " + tokens[1].trim());
			}

		}

		if (!values.isEmpty())
		{
			days.add(values.toArray(new Double[0]));
		}

		reader.close();

		return days;
	}

	/**
	 * Reads and loads the data from the text file
	 * @param filename The name of the file to be read
	 * 
	 * @return days The data in an ArrayList<Double[]> format
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static ArrayList<Double[]> loadData(String filename)
			throws FileNotFoundException, IOException {

		BufferedReader reader = new BufferedReader(new FileReader(filename));

		String line = null;

		ArrayList<Double[]> days = new ArrayList<Double[]>();
		ArrayList<Double> values = new ArrayList<Double>();
		String timestamp = null;

		while ((line = reader.readLine()) != null)
		{
			String[] tokens = line.split("\\s");

			if (timestamp == null)
			{
				timestamp = tokens[0].trim();
			}
			else if (!tokens[0].trim().equals(timestamp))
			{
				days.add(values.toArray(new Double[0]));
				values.clear();

				timestamp = tokens[0].trim();
			}

			//if it does not contain the 10min string, then the file is tick data; if it contains the 10min string, then we go to the else-statement, as the file is 10-min data
			if (!filename.contains("10min"))
				values.add(Double.parseDouble(tokens[2].trim()));
			else{
				double openBid = Double.parseDouble(tokens[2].trim());//unused
				double openAsk = Double.parseDouble(tokens[3].trim());//unused
				double closeBid = Double.parseDouble(tokens[4].trim());
				double closeAsk = Double.parseDouble(tokens[5].trim());
				double midOpen = (openBid + openAsk) / 2;//unused
				double midClose = (closeBid + closeAsk) / 2;
				DecimalFormat df = new DecimalFormat("#.########");

				//we are trading using the closing midPrice at the end of the 10-min interval.
				values.add(Double.parseDouble(df.format(midClose)));
			}
		}

		if (!values.isEmpty())
		{
			days.add(values.toArray(new Double[0]));
		}

		/*
        data = new double[values.size()];

        for (int i = 0; i < data.length; i++)
        {
            data[i] = values.get(i);
        }
		 */

		reader.close();

		return days;
	}

}
