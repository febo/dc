/**
* This file is part of the directional changes DC+GA project.
*
* DC+GA is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DC+GA is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DC+GA.  If not, see <http://www.gnu.org/licenses/>.

*/

package dc.io;

import java.io.File;
import java.util.ArrayList;

import files.FWriter;

/** Logger Class **/
public class Logger {
	
	FWriter writer;
	String folder;
	
	/** Constructor. Creating files inside a folder related to the training and testing index of the data **/
	public Logger(String fileName, String trainingIndex, String testingIndex){
	
		folder = fileName + "/i" + trainingIndex + "_" + testingIndex + "/";
		File f = new File(folder);
		f.mkdirs();
		
		writer = new FWriter(folder + "Curves.txt");
		writer = new FWriter(folder + "Results.txt");
		writer = new FWriter(folder + "Logger.txt");
		writer = new FWriter(folder + "Solutions.txt");
		writer = new FWriter(folder + "EquityCurve.txt");
		writer = new FWriter(folder + "Fitness.txt");
	}
	
	/** Empty constructor */
	public Logger(){		
		writer = new FWriter(folder + "Curves.txt");
		writer = new FWriter(folder + "Results.txt");
		writer = new FWriter(folder + "Logger.txt");
		writer = new FWriter(folder + "Solutions.txt");
		writer = new FWriter(folder + "EquityCurve.txt");
		writer = new FWriter(folder + "Fitness.txt");
	}
	
	/** Saving a string element */
	public void save(String fileName, String toSave){
		writer.openToAppend(new File(folder + fileName));
		writer.write(toSave);
		writer.closeFile();
	}
	
	/** Saving ArrayList elements */
	public void save(String fileName, ArrayList<String> toSave){
		writer.openToAppend(new File(folder + fileName));
		for (String s:toSave)
			writer.write(s);
		writer.closeFile();
	}
}